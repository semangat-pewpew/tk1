**Anggota Kelompok :**
1. Aulia Radityatama Suhendra (1906399865)
2. Fareeha Nisa Zayda Azeeza (1906399644)
3. Fariz Wahyuzan Dwitilas (1906399511)
4. Ian Andersen Ng (1906400280)
5. Sultan Daffa Nusantara (1906399581)

**Link HerokuApp :**
http://semangatppw.herokuapp.com

**Cerita aplikasi yang diajukan serta kebermanfaatannya**
Kami membuat website yang menyediakan berbagai informasi tentang COVID-19 
yang sedang mewabah sekarang. Kami menyediakan hotline . Kami juga menyediakan 
form yang dapat diisi pengunjung jika mereka menginginkan bantuan. 
Website kami menyediakan peta indonesia yang memperlihatkan zona-zona yang 
mempunyai persentase penularan yang tinggi 

**Daftar fitur yang akan diimplementasikan**
- Hotzone - peta penularan 
- Pencegahan corona
- Hotline 
- Hal yang bisa dilakukan sendiri 
- FAQ
- Contact Us 
